from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from .models import Post, Comment, ProgrammingSection
from .forms import PostForm, CommentForm, MessageToAdminForm

# Create your views here.

def post_list(request):
    # Gets all posts from the model which are already published
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    response = render(request, 'blog/post_list.html', {'posts': posts})
    return response


def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/post_detail.html', {'post': post})

@login_required
def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            #post.published_date = timezone.now() We don't want to publish instantly
            post.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'blog/post_edit.html', {'form': form})


@login_required
def post_draft_list(request):
    posts = Post.objects.filter(published_date__isnull=True).order_by('created_date')
    return render(request, 'blog/post_draft_list.html', {'posts': posts})

@login_required
def post_publish(request, pk):
    post = get_object_or_404(Post, pk=pk)
    post.publish()
    return redirect('post_detail', pk=pk)

@login_required
def post_remove(request, pk):
    post = get_object_or_404(Post, pk=pk)
    post.delete()
    return redirect('post_list')

def add_comment_to_post(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if (request.method == "POST"):
        form = CommentForm(request.POST)
        if (form.is_valid()):
            comment = form.save(commit=False)
            comment.post = post
            comment.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = CommentForm()
    return render(request, 'blog/add_comment_to_post.html', {'form': form})

@login_required
def comment_approve(request, pk):
    comment = get_object_or_404(Comment, pk=pk)
    comment.approve()
    return redirect('post_detail', pk=comment.post.pk)

@login_required
def comment_remove(request, pk):
    comment = get_object_or_404(Comment, pk=pk)
    comment.delete()
    return redirect('post_detail', pk=comment.post.pk)

# About Page

def about(request):
    return render(request, 'blog/about.html')


# Admin Contact Form
def admin_contact(request):
    if request.method == "POST":
        form = MessageToAdminForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            messages.success(request, 'Form submission successful')
            return redirect('post_list')
    else:
        form = MessageToAdminForm()
    return render(request, 'blog/admin_contact.html', {'form': form})


def logoutView(request):
    logout(request)
    messages.success(request, 'Logged out successfully')
    return redirect('post_list')

def programming(request):
    objects = ProgrammingSection.objects.all()
    return render(request, 'blog/programming.html', { 'PSOs':objects })

def twitter(request):
    return render(request, 'blog/twitter.html', {})