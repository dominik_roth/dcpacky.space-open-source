from django.contrib import admin
from .models import Post, Comment, adminMessage, ProgrammingSection

# Register your models here.
admin.site.register(Post)
admin.site.register(Comment)
admin.site.register(ProgrammingSection)

admin.site.register(adminMessage)
