from django.db import models
from django.utils import timezone

from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.


class Post(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    materialize_icon = models.CharField(max_length=100, default='comment')
    text = models.TextField()
    created_date = models.DateTimeField(default = timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    
    def publish(self):
        self.published_date = timezone.now()
        self.save()
        
    def __str__(self):
        return self.title
    

class Comment(models.Model):
    post = models.ForeignKey('blog.Post', on_delete=models.CASCADE, related_name='comments')
    author = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    approved_comment = models.BooleanField(default=False)
    
    def approved_comments(self):
        return self.comments.filter(approved_comment=True)
    
    def approve(self):
        self.approved_comment = True
        self.save()
        
    def __str__(self):
        return self.text
    

class adminMessage(models.Model):
    byName = models.CharField(max_length=200)
    title = models.CharField(max_length=200)
    text = models.TextField()
    email = models.CharField(max_length=200)
    created_date = timezone.now
    
    def __str__(self):
        return self.title


class ProgrammingSection(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    link = models.CharField(max_length=200)
    
    def __str__(self):
        return self.title
#class Profile(models.Model):
#