from django.urls import path
from django.conf.urls import url, include
from . import views

urlpatterns = [
    path('', views.post_list, name='post_list'),
    path('about', views.about, name='about'),


    path('post/<int:pk>/', views.post_detail, name='post_detail'),
    path('post/new', views.post_new, name='post_new'),
    url(r'^drafts/$', views.post_draft_list, name='post_draft_list'),
    url(r'^post/(?P<pk>\d+)/publish/$', views.post_publish, name="post_publish"),
    url(r'^post/(?P<pk>\d+)/remove/$', views.post_remove, name='post_remove'),
    url(r'^post/(?P<pk>\d+)/comment/$', views.add_comment_to_post, name='add_comment_to_post'),
    url(r'^comment/(?P<pk>\d+)/approve/$', views.comment_approve, name='comment_approve'),
    url(r'^comment/(?P<pk>\d+)/remove/$', views.comment_remove, name='comment_remove'),
    
    url('logout', views.logoutView, name='logout'),
    
    path('admin_contact', views.admin_contact, name='admin_contact'),
    url(r'^captcha/', include('captcha.urls')),
    
    path('programming', views.programming, name='programming'),
    path('twitter', views.twitter, name='twitter'),
]